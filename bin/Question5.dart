import 'dart:math';

Question5() {
  List<int> A = [1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0];
  int K = 2;
  var maxOnes = A.reduce(min);
  int numConv = 0;
  int left = 0, right = 0;

  for (right = 0; right < A.length; right++) {
    if (A[right] == 0) {
      numConv++;
    }

    while (numConv > K) {
      if (A[left] == 0) {
        numConv--;
      }
      left++;
    }
    maxOnes = max(maxOnes, right - left + 1);
  }
  print(maxOnes);
  return maxOnes;
}
